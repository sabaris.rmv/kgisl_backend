const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const config = require("./configurations/configuration.js");

const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

mongoose.Promise = global.Promise;
mongoose
  .connect("mongodb+srv://admin:xzlZCpaJ65a0cZmB@cluster0.jxfim.mongodb.net/meanassessment?retryWrites=true&w=majority", {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => {
    console.info("Successfully connected to the database");
  })
  .catch((err) => {
    console.error("Could not connect to the database. Exiting now...");
    process.exit();
  });

require("./routes/routes.js")(app);

app.listen(config.port, () => {
  console.info("Server is starting up in port : " + config.port);
});
