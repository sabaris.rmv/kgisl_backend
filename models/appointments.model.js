const mongoose = require("mongoose");
var mongooseAggregatePaginate = require("mongoose-aggregate-paginate");

const appointmentSchema = mongoose.Schema({
  id: mongoose.Types.ObjectId,
  name: String,
  email: String,
  phone: Number,
  timeSlots:{type: mongoose.Types.ObjectId, ref: 'timeSlot'},
  created_at: Date
});

appointmentSchema.plugin(mongooseAggregatePaginate);
const appointment = mongoose.model('appointment', appointmentSchema);
module.exports = appointment;
