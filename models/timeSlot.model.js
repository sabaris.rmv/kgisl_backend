const mongoose = require("mongoose");
var mongooseAggregatePaginate = require("mongoose-aggregate-paginate");

const timeSlotSchema = mongoose.Schema({
    appointment_time: String,
    appointment_date: String,
    created_at: Date
});

timeSlotSchema.plugin(mongooseAggregatePaginate);
const timeSlot = mongoose.model('timeSlot', timeSlotSchema);
module.exports = timeSlot;
