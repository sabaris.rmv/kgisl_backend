const variableCheck = (value) => {
  if (value != null && value != undefined && value != "") {
    return true;
  } else {
    return false;
  }
};
exports.variableCheck = variableCheck;

const variableCheckAsArray = (value) => {
  if (value != null && value != undefined && value != "") {
    if (value.length > 0) {
      return value.length;
    } else {
      return 0;
    }
  } else {
    return -1;
  }
};
exports.variableCheckAsArray = variableCheckAsArray;

const variableCheckAsBoolean = (value) => {
  if (value != null && value != undefined && value != "" && typeof value === "boolean") {
    return value;
  } else {
    return false;
  }
};
exports.variableCheckAsBoolean = variableCheckAsBoolean;
