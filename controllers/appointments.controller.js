const appointmentModel = require("../models/appointments.model.js");
const timeSlotModel = require("../models/timeSlot.model.js");

exports.createAppointment = (req, res) => {

    // JSON = {
    //     email:"",
    //     phone:"",
    //     name:"",
    //     appointment_time:"",
    //     appointment_date:"",
    // }

    var requestBody = req.body;

    timeSlotModel.find({
        // email: requestBody.email,
        // phone: requestBody.phone,
        appointment_time: requestBody.appointment_time,
        appointment_date: requestBody.appointment_date
    }, (err, appointment) => {
        console.log(appointment)
        if (appointment.length > 0) {
            res.status(400).json({
                apiName: "Appointment Create API",
                success: false,
                message: "This time slot already booked.",
            });
        } else {
            var newsTimeSlot = new timeSlotModel({
                appointment_time: requestBody.appointment_time,
                appointment_date: requestBody.appointment_date,
                created_at: Date.now()
            });
            newsTimeSlot.save((err, savedTimeSlot) => {
                if (err) {
                    res.status(400).json({
                        apiName: "Appointment Create API",
                        success: false,
                        message: "Error occurred while make appointment",
                    });
                } else {
                    var newappointment = new appointmentModel({
                        name: requestBody.name,
                        email: requestBody.email,
                        phone: requestBody.phone,
                        timeSlots: savedTimeSlot._id
                    });

                    newappointment.save((err, savedAppoitment) => {
                        if (err) {
                            newsTimeSlot.find({
                                _id: savedTimeSlot._id
                            }).remove().exec(function (err, data) {
                                console.log("time slot is removed.");
                                res.status(400).json({
                                    apiName: "Appointment Create API",
                                    success: false,
                                    message: "Error occurred while make appointment",
                                });
                            })
                        } else {
                            res.json({
                                apiName: "Appointment Create API",
                                success: true,
                                message: "Appointment created successfully",
                            });
                        }
                    });
                }
            });
        }
    })
}

exports.appointmentList = (req, res) => {
    console.log("Appointement List")

    var aggregate = timeSlotModel.aggregate([{
            $lookup: {
                from: "appointments",
                localField: "_id",
                foreignField: "timeSlots",
                as: "patient",
            },
        },
        {
            $unwind: '$patient'
        },
        {
            $project: {
                appointment_time:1,
                appointment_date:1,
                created_at:1,
                patient: "$patient"
            }
        }
        // {
        //     $arrayElemAt: ["$appointments", 0]
        // }
    ]);

    var options = {
        page: req.query.page || 1,
        limit: parseInt(req.query.limit) || 50,
    };

    timeSlotModel.aggregatePaginate(aggregate, options, function (
        err,
        listdata,
        pageCount,
        count
    ) {
        if (err) {
            console.log(err)
            res.status(400).json({
                apiName: "Appointment List API",
                success: false,
                message: "Some Error Occured",
            });
        } else {
            res.json({
                apiName: "Appointment List API",
                success: true,
                message: "Successfully View List",
                list: listdata,
                currentPage: options.page,
                limit: options.limit,
                pageCount: pageCount,
                totalItem: count
            });
        }
    });
}