const appointmentController = require('../controllers/appointments.controller.js');
// const slotController = require('../controllers/slot.controller.js')

module.exports = (app) => {
    app.use(function (req, res, next) {
        //allow cross origin requests
        res.setHeader(
            "Access-Control-Allow-Methods",
            "POST, PUT, OPTIONS, DELETE, GET"
        );
        res.header("Access-Control-Allow-Origin", "*");
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, x-access-token, X-Requested-With, Content-Type, Accept"
        );
        res.header("Access-Control-Allow-Credentials", true);
        next();
    });

    app.get('/api/v1/appointments', appointmentController.appointmentList);
    // app.get('/slote', slotController.all);
    app.post('/api/v1/appointments', appointmentController.createAppointment);

    app.get('/', function (req, res) {
        res.send('Patient Application...');
    })
}